const fs = require('fs');
const axios = require('axios');
const {get, chain} = require('lodash');

axios.get('https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/metadata/icons.json').then(function (response) {
    convertData(response.data);
});

function convertData(data) {
    const results = new Array;
    for (const name in data) {
        const icon = get(data, name);
        const aliases = get(icon, 'aliases.names', new Array);
        aliases.push(name);
        const search = chain(icon.search.terms).concat(aliases).map(function (term) {
            const match = new RegExp('-', 'g');
            return term.replace(match, ' ').toLowerCase();
        }).uniq();
        for (const style of icon.free) {
            results.push({
                name: name,
                class: getBrandClass(name, style),
                style: style,
                search: search,
                unicode: icon.unicode,
                label: icon.label
            });
        }
    }
    writeJsonFile('data.json', results);
}

function writeJsonFile(name, data) {
    const raw = JSON.stringify(data, null, 4);
    fs.writeFileSync(name, raw);
}

function getBrandClass(index, value) {
    switch (value) {
        case 'solid':
            return `fas fa-${index}`;
        case 'regular':
            return `far fa-${index}`;
        case 'brands':
            return `fab fa-${index}`;
    }
}